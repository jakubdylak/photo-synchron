from utils import photo_synchron_utils as psu

if __name__ == "__main__":

    extensions = [".jpg", ".png", ".jpeg", ".mp4", ".raw"]
    count = 0

    psu.welcome()

    while True:
        key = str(input(">>> "))

        if key == "1":
            target_path = psu.get_dir()
            if len(target_path) == 0:
                print("\n--- No path selected. ---\n")
            else:
                print(f"\nPC: {target_path}\n")

        elif key == "2":
            source_path = psu.get_dir()
            if len(source_path) == 0:
                print("\n--- No path selected. ---\n")
            else:
                print(f"\nDevice: {source_path}\n")

        elif key == "3":
            try:
                diff_files = psu.compare_folders(target_path, source_path, extensions)
            except NameError:
                print("\n--- Select proper paths first. ---\n")

        elif key.lower() == "c":
            try:
                if len(diff_files) == 0:
                    print("\n--- No files to copy. ---\n")

                else:
                    copy_path = psu.get_dir()
                    if len(copy_path) == 0:
                        print("\nNo path selected.\n")
                    else:
                        print(f"\nCopy path: {copy_path}\n")
                        count += psu.copy_files(source_path, diff_files, copy_path)
            except NameError:
                print("\n--- Select proper paths and compare them first. ---\n")

        elif key.lower() == "a":
            try:
                if len(diff_files) == 0:
                    print("\n--- No files to copy. ---\n")
                else:
                    count += psu.copy_files_to_year(
                        target_path, source_path, diff_files
                    )
            except NameError:
                print("\nSelect proper paths and compare them first.\n")

        elif key.lower() == "q":
            print(f"\n--- Quitting program. {count} files were copied. ---\n")
            psu.count_down(2)
            break

        elif key == "?":
            psu.help()

        else:
            print("\nChoose right command or [?] for help.\n")
