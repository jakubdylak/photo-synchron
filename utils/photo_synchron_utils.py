import os
import sys
import time
import shutil
from tkinter.filedialog import askdirectory
from tqdm import tqdm
from PIL import Image


def welcome():
    welcome_txt = (
        "\n-------------------\n Photo Synchron v0.1\n-------------------\n\n"
        "Welcome to Photo Synchron!\n\n"
        "This program allows you to synchronize photos and movies from external device with your PC.\n"
        "*** Devices such as Android phones and Iphones are not supported yet. ***\n\n"
        "Photo Synchron will compare two directories paying attention to file names and extensions.\n"
        "Program looks for images and movies in subfolders too!\n\n"
        "There are two copy modes:\n"
        "- simple copy to desired path,\n"
        "- Anitka Mode - create folder based on files's modification year and copy to appropriete folders.\n\n"
        "Supported extensions are: .jpg, .png, .jpeg, .raw and .mp4.\n\n"
        "Photo Synchron commands:\n"
        "[1] - select path to folder on PC\n"
        "[2] - select path to folder on external device\n"
        "[3] - compare folders\n"
        "[C] - copy files to selected folder\n"
        "[A] - Anitka mode\n"
        "[?] - help\n"
        "[Q] - exit program.\n\n"
        "Choose one and press Enter.\n\n"
    )
    print(welcome_txt)


def help():
    help_txt = (
        "Photo Synchron v0 HELP:\n\n"
        "\nFor better performance please keep the order of commands as shown below.\n\n"
        "Photo Synchron commands:\n"
        "[1] - select path to folder on PC containing all images/movies\n"
        "[2] - select path to folder on external device containing all images/movies\n"
        "[3] - compare folders\n"
        "[C] - copy files to selected folder\n"
        "[A] - create folder based on files's modification year if it does not exits and copy files\n"
        "[?] - help\n"
        "[Q] - exit program.\n"
        "Choose one and press Enter.\n\n"
    )
    print(help_txt)


def get_dir():
    """Obtains and return string of path to selected directory"""
    # get current working directory
    work_dir = os.getcwd()
    # obtain path to desired directory
    dir_path = askdirectory(initialdir=work_dir)

    return dir_path


def search_files(path: str, extensions: list):
    """
    Search for files with given extensions.
    Return list of file names with extensions.
    """
    files_found = []
    ext = tuple(extensions)
    # append files with given extensions to list
    for _, _, files in os.walk(path):
        for f in files:
            if f.lower().endswith(ext):
                files_found.append(f)

    return files_found


def compare_folders(target_path: str, source_path: str, extensions: list):
    """
    Compares files from two given paths and returns list of files
    that are not present in target path.

    Parameters:
    target_path - path of folder with photos on your Mac or PC
    source_path - path of folder with photos from your phone
    """
    # create empty lists to append files
    target_files = search_files(target_path, extensions)
    source_files = search_files(source_path, extensions)

    # make list of source files that are not in target path
    diff_files = [f for f in source_files if f not in target_files]

    if len(diff_files) > 0:
        print(f"\n--- Found {len(diff_files)} files to copy. ---\n")
    else:
        print("\n--- No files to copy. ---\n")

    # ------ add showing files to copy feature? ------

    return diff_files


def get_files_paths(source_path: str, diff_files: list):
    """
    Get full file path.
    Return list of full paths.
    """
    diff_paths = []
    for root, _, files in os.walk(source_path):
        for f in files:
            if f in diff_files:
                diff_paths.append(os.path.join(root, f))

    return diff_paths


def copy_files(source_path: str, diff_files: list, copy_path: str):
    """
    Copy files to selected folder.
    Returns number of files copied.
    """

    count = 0

    # make list of files to copy with their full path
    diff_paths = get_files_paths(source_path, diff_files)

    should_copy = input(f"\nCopy {len(diff_paths)} files to {copy_path} [y/N] ").lower()

    if should_copy.lower() == "y":
        print("\n")
        # copy with metadata
        for d in tqdm(diff_paths):
            shutil.copy2(d, copy_path)
            sys.stdout.flush()
            count += 1
        print(f"\n\n--- Successfully copied {count} files. ---\n")
    else:
        print(f"\n--- {count} files copied. ---\n")

    return count


def paths_year(paths: list) -> list:
    """
    Make list of file creation year.
    """
    # create list of modification year for each file
    years = [time.ctime(os.path.getmtime(path))[-4:] for path in paths]

    years = []

    for p in paths:
        try:
            years.append(Image.open(p)._getexif()[36867][:4])
        except:
            years.append(time.ctime(os.path.getmtime(p))[-4:])
        

    return years


def copy_files_to_year(target_path: str, source_path: str, diff_files: list):
    """
    Create folders based on file modification year in target path.
    Copy files to appropriate year folder.
    Returns number of files copied.
    """

    count = 0

    # make list of files to copy with their full path
    diff_paths = get_files_paths(source_path, diff_files)

    # create list of years of file modification
    years = paths_year(diff_paths)

    should_copy = input(
        f"\nCopy {len(diff_paths)} files to appropriete year folder in {target_path} [y/N] "
    ).lower()

    if should_copy.lower() == "y":
        print("\n")
        # make new folder based on year if it doesn't exist in target destination
        for year in set(years):
            new_dir = os.path.join(target_path, year)
            os.makedirs(new_dir, exist_ok=True)

        # copy with metadata
        years_diffs = list(zip(years, diff_paths))
        for y, d in tqdm(years_diffs):
            shutil.copy2(d, os.path.join(target_path, y))
            sys.stdout.flush()
            count += 1
        print(f"\n\n--- Successfully copied {count} files. ---\n")
    else:
        print(f"\n--- {count} files copied. ---\n")

    return count


def count_down(number: int):
    """
    Print given number of dots and "Bye!" in the end.
    """
    while number:
        print("." * number)
        sys.stdout.flush()
        time.sleep(1)
        number -= 1
        if number == 0:
            print("Bye!")
