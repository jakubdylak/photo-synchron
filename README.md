# ![alt text][logo] **Photo Synchron** 

[logo]: https://gitlab.com/jakubdylak/photo-synchron/-/raw/master/icon/icon.png


Photo synchron is a user friendly program to synchronize photo and video files between PC and external devices. It compares each file by name and extension. Supported extensions (so far) are: *.jpg, .png, .jpeg, .raw and .mp4*.


## **Technologies**

Photo Synchron was made with Python 3.10.5 and libraries such as:
- os,
- sys,
- time,
- shutil,
- tkinter.

Project includes third-party libraries like:
- [tqdm](https://github.com/tqdm/tqdm)
- [Pillow](https://github.com/python-pillow/Pillow)

Exe file for Windows users was generated with [Auto PY to EXE](https://github.com/brentvollebregt/auto-py-to-exe).


## **Usage**

### **How to run Photo Synchron?**

Photo Synchron can be run in command-line shell:

```
python photo_synchron.py
```
or
```
python3 photo_synchron.py
```

Windows users can just use prepared exe file.

### **How to use Photo Synchron?**

First thing to do is to select proper paths and compare files between then in order to search for files that are not present in destination path. After this user can choose two copy modes:
1. Normal Mode  - copy files to selected folder.
2. Anitka Mode - check files creation year, create folder for each year(if does not exist) and copy files to appropriate folder. 


## **Roadmap**

This project is still under development. Below are some features that will be implemented in the future:
- Version 1.0 will have GUI.
- Executable files for Mac OS and Linux.
- Possibility for user to pick extensions (and adding more to choose from).
- Support for Android and iOS devices.


## **Feedback**

If you find any bug, have some ideas or want feature to be added in next Photo Synchron version feel free to send me any feedback on jakubdylak@gmail.com.

Take care,
JD

